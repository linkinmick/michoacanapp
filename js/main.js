inicializa();

var total = 0;

$(".productos-contenedor").hide();



//detectar click o tap en categorias
var catNives = $('#catNieves'), catBotanas = $('#catBotanas'); 
catNives.on('click', function(e){
	$(".productos-contenedor").hide();
	$(".contenedor-nieves").show();
});
catBotanas.on('click', function(e){	
	$(".productos-contenedor").hide();
	$(".contenedor-botanas").show();
});

$("div.producto").on("click", function(){
	$(this).toggleClass("agregado");
	var precio = $(this).children(".producto-precio").attr("precio");
	precio = parseInt(precio);


	if($(this).hasClass("agregado")){//si agregado suma
		total += precio;
	}else{//sino resta
		total -= precio;
	}

	$("#total").val("$"+total);

});

function inicializa(){
	var categorias = cargaCategorias();

	for(var c in categorias){
		//console.log(categorias[c]);
		cargaProductos(categorias[c]);
	}

}

function cargaCategorias(){
	var categorias = ["nieves", "botanas"];
	return categorias;
}

function cargaProductos(producto){
	//precargar productos
	$.ajax({
		async: false,
	  	type: 'GET',
	  	url: 'productos/'+producto+'.json',
	  	// type of data we are expecting in return:
	  	dataType: 'json',
	  	timeout: 300,
	  	success: function(data){
	  		//console.log(data);
			var prodCnt = $('<div></div>').addClass("productos-contenedor contenedor-"+producto);

			for(var d in data){
				//console.log(data[d].nm);			
				var prodW = $('<div></div>').addClass("producto producto-"+producto.substring(0,producto.length-1));
				var prodNM = $('<div></div>').addClass("producto-nm");			
				var prodPrecio = $('<div></div>').addClass("producto-precio");
					prodNM.html(data[d].nm);
				prodPrecio.attr("precio",data[d].precio).html("$ "+data[d].precio);
				prodW.append(prodNM);
				prodW.append(prodPrecio);
				prodCnt.append(prodW);
			}

			$('#productos').append(prodCnt);
	  	},
	  	error: function(xhr, type){
	    	alert('Ajax error!');
	  	}
	});
}